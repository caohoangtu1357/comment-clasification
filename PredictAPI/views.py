# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
import numpy as np
import os
import sklearn
from sklearn.feature_extraction.text import CountVectorizer
import joblib
import json
from django.http import JsonResponse
import unidecode


dirpath = os.path.dirname(os.path.abspath(__file__))
dataPath = os.path.join(dirpath, "model/ModelCommentGoodBad.sav")
model = joblib.load(dataPath)

dataPath = os.path.join(dirpath, "model/x_train.npy")
x_train = np.load(dataPath)

dataPath = os.path.join(dirpath, "model/y_train.npy")
y_train = np.load(dataPath)


vectorizer = CountVectorizer()
vectorizer.fit(x_train)
vectorizer.transform(x_train)


def cleanData(str):
    return unidecode.unidecode(str).lower()

def predictComment(request):

    

    y = json.loads(request.body)
    comment = y['comment']
    print(comment)
    comment_vect = vectorizer.transform([cleanData(comment)])
    
    return JsonResponse(
        {
            "predict":int(model.predict(comment_vect)[0]),
            "predict_proba_bad":model.predict_proba(comment_vect)[0][0],
            "predict_proba_good":model.predict_proba(comment_vect)[0][1]
        }
    )
