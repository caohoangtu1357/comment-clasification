from django.apps import AppConfig


class PredictapiConfig(AppConfig):
    name = 'PredictAPI'
